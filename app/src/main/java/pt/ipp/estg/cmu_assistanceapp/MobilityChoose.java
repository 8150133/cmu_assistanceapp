package pt.ipp.estg.cmu_assistanceapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MobilityChoose extends AppCompatActivity {
    ImageButton accelerometer, tracker;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choosemobility_layout);

        accelerometer =findViewById(R.id.accelerometer);
        accelerometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToAccelerometer = new Intent(MobilityChoose.this, Accelerometer.class);
                startActivity(intentToAccelerometer);
            }
        });

        tracker = findViewById(R.id.tracker);
        tracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToTracker = new Intent(MobilityChoose.this, Tracker.class);
            }
        });

    }
}

package pt.ipp.estg.cmu_assistanceapp.API;

import java.util.List;

import pt.ipp.estg.cmu_assistanceapp.Models.StreetMap;
import pt.ipp.estg.cmu_assistanceapp.Models.lookup.LookUp;
import pt.ipp.estg.cmu_assistanceapp.Models.place.PlaceDetail;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;

public interface OSMAPI {

    String url = "https://nominatim.openstreetmap.org/";

    @Headers("Content-Type: application/json")
    @GET
    Call<List<StreetMap>> getSeachCity(@Url String url);

    @Headers("Content-Type: application/json")
    @GET
    Call<PlaceDetail> getLocationDetail(@Url String url);


    @Headers("Content-Type: application/json")
    @GET
    Call<List<LookUp>> getLocationDetailFromLookUp(@Url String url);

}

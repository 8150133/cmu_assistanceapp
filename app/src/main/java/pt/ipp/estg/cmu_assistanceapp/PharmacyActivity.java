package pt.ipp.estg.cmu_assistanceapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pt.ipp.estg.cmu_assistanceapp.API.APIClient;
import pt.ipp.estg.cmu_assistanceapp.API.OSMAPI;
import pt.ipp.estg.cmu_assistanceapp.API.PharmacyAdapter;
import pt.ipp.estg.cmu_assistanceapp.Adapters.SearchAdapterPharmacy;
import pt.ipp.estg.cmu_assistanceapp.Models.StreetMap;
import pt.ipp.estg.cmu_assistanceapp.Utils.Connectivity;
import pt.ipp.estg.cmu_assistanceapp.Utils.KeyboardUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PharmacyActivity extends AppCompatActivity {
    OSMAPI osmAPI;
    RecyclerView rvPharmacies;
    PharmacyAdapter pharmaciesAdapter;

    EditText editTextSearch;
    ImageView imageView;
    SearchAdapterPharmacy searchAdapterPharmacies;
    String cityName;
    ProgressDialog progressDoalog;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmacias);
        Intent intent = getIntent();
        editTextSearch =findViewById(R.id.editTextId);
        imageView=findViewById(R.id.imageView);

        rvPharmacies = (RecyclerView) findViewById(R.id.rvPharmacies);
        rvPharmacies.setHasFixedSize(true);
        rvPharmacies.setLayoutManager(new LinearLayoutManager(this));

        RecyclerView.ItemDecoration decorationItem = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        //  rvPharmacies.addItemDecoration(decorationItem);

        osmAPI = APIClient.getClient().create(OSMAPI.class);

        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    KeyboardUtils.hideSoftKeyboard(editTextSearch,PharmacyActivity.this);

                    cityName=v.getText().toString().trim()+"+pharmacy";
                    getSearchResultApi(cityName);
                    return true;
                }
                return false;
            }
        });

    }

    public void showProgress(){
        progressDoalog = new ProgressDialog(PharmacyActivity.this);
        progressDoalog.setMessage("Its loading....");
        progressDoalog.setTitle("Wait");
        progressDoalog.show();
    }
    public void dissmissProgress(){
        if(progressDoalog!=null){
            if(progressDoalog.isShowing()){
                progressDoalog.dismiss();
            }
        }
    }
    public void getSearchResultApi(String cityName){
        if(Connectivity.isConnected(this)) {
            showProgress();
            imageView.setVisibility(View.GONE);
            rvPharmacies.setVisibility(View.VISIBLE);
            Call<List<StreetMap>> callUrl = osmAPI.getSeachCity("/search?q=" + cityName + "&format=json");

            callUrl.enqueue(new Callback<List<StreetMap>>() {
                @Override
                public void onResponse(Call<List<StreetMap>> call, Response<List<StreetMap>> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            searchAdapterPharmacies = new SearchAdapterPharmacy(PharmacyActivity.this, response.body());
                            rvPharmacies.setAdapter(searchAdapterPharmacies);
                        }
                    }
                    dissmissProgress();

                }
                @Override
                public void onFailure(Call<List<StreetMap>> call, Throwable t) {
                    dissmissProgress();
                    Toast.makeText(getApplicationContext(), "Error in Response:" + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Toast.makeText(PharmacyActivity.this,"Your internet connection seems off!",Toast.LENGTH_SHORT).show();
        }

    }
}

package pt.ipp.estg.cmu_assistanceapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class WhatToChoose extends AppCompatActivity {

    ImageButton mobility, memory, nutrition, pills, pharmacy_choose;
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooseoption);

        pharmacy_choose =findViewById(R.id.pharmacyList);
        pharmacy_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToPharmacies = new Intent(WhatToChoose.this,PharmacyActivity.class);
                startActivity(intentToPharmacies);
            }
        });

        memory = findViewById(R.id.memory);
        memory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToMemoryGames = new Intent(WhatToChoose.this, GameChoose.class);
                startActivity(intentToMemoryGames);
            }
        });

        mobility = findViewById(R.id.mobility);
        mobility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToMobility = new Intent(WhatToChoose.this, MobilityChoose.class);
                startActivity(intentToMobility);
            }
        });

        pills = findViewById(R.id.pills);
        pills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toRegistMed = new Intent(WhatToChoose.this, RegisterMedActivity.class);
                startActivity(toRegistMed);
            }
        });

    }
}

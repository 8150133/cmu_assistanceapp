package pt.ipp.estg.cmu_assistanceapp;

public class QuestionModel {
    public String questions[] = {
            "1+1?",
            "8*8?",
    };

    private String choices[][] = {
            {"2","3","4","5"},
            {"68","62","64","66"},
    };

    private String correctAnswers[] = {"2","64"};

    public String getQuestion(int a){
        String question = questions[a];
        return question;
    }

    public String getChohice1(int a){
        String choice = choices[a][0];
        return choice;
    }

    public String getChohice2(int a){
        String choice = choices[a][1];
        return choice;
    }

    public String getChohice3(int a){
        String choice = choices[a][2];
        return choice;
    }

    public String getChohice4(int a){
        String choice = choices[a][3];
        return choice;
    }

    public String getCCorrectAnswer(int a){
        String answer = correctAnswers[a];
        return answer;
    }
}

package pt.ipp.estg.cmu_assistanceapp;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.icu.text.DecimalFormat;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

public class Accelerometer extends Activity implements SensorEventListener {
    private SensorManager sensorManager;

    TextView xCoor; // declare X axis object
    TextView yCoor; // declare Y axis object
    TextView zCoor; // declare Z axis object

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.accelerometer_layout);

        xCoor=(TextView)findViewById(R.id.xcoor); // create X axis object
        yCoor=(TextView)findViewById(R.id.ycoor); // create Y axis object
        zCoor=(TextView)findViewById(R.id.zcoor); // create Z axis object

        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        // add listener. The listener will be  (this) class
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);


    }

    public void onAccuracyChanged(Sensor sensor,int accuracy){

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onSensorChanged(SensorEvent event){

        // check sensor type
        if(event.sensor.getType()== Sensor.TYPE_ACCELEROMETER){

            // assign directions
            double x=event.values[0];
            double y=event.values[1];
            double z=event.values[2];

            double accelerationReader = Math.sqrt(Math.pow(x, 2)
                    + Math.pow(y, 2)
                    + Math.pow(z, 2));

            xCoor.setText("X: "+x);
            yCoor.setText("Y: "+y);
            zCoor.setText("Z: "+z);
        }
    }
}
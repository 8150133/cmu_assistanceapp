package pt.ipp.estg.cmu_assistanceapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pt.ipp.estg.cmu_assistanceapp.MapaPrincipalPharmacies;
import pt.ipp.estg.cmu_assistanceapp.Models.StreetMap;
import pt.ipp.estg.cmu_assistanceapp.R;

public class SearchAdapterPharmacy extends RecyclerView.Adapter<SearchAdapterPharmacy.ViewHolder>{
    private Context mContext;
    private List<StreetMap> searchList;

    public SearchAdapterPharmacy(Context mContext, List<StreetMap> searchList) {
        this.mContext = mContext;
        this.searchList = searchList;
    }

    @NonNull
    @Override
    public SearchAdapterPharmacy.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View pharmacyView = inflater.inflate(R.layout.item_search_layout_pharmacies, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(pharmacyView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapterPharmacy.ViewHolder viewHolder, int i) {
        final StreetMap apiPC = searchList.get(i);

        viewHolder.namePOI.setText("Name : "+apiPC.getDisplayName());
        viewHolder.latitude.setText("latitude : "+apiPC.getLat());
        viewHolder.longitude.setText("longitude : "+apiPC.getLon());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //vai buscar a letra em maiuculo do type para depois acrescentar ao osm_ids na api lookup
                String osm_ids=""+apiPC.getOsmType().toUpperCase().charAt(0)+apiPC.getOsmId();

                Intent intent = new Intent(mContext, MapaPrincipalPharmacies.class);
                intent.putExtra("place_id",apiPC.getPlaceId());
                intent.putExtra("latitude_id",apiPC.getLat());
                intent.putExtra("longitude_id",apiPC.getLon());
                intent.putExtra("display_name",apiPC.getDisplayName());
                intent.putExtra("osm_ids_key",osm_ids);

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView namePOI;
        public TextView latitude;
        public TextView longitude;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            namePOI = (TextView) itemView.findViewById(R.id.nomePOI);
            latitude = (TextView) itemView.findViewById(R.id.latitude);
            longitude = (TextView) itemView.findViewById(R.id.longitude);
        }
    }
}
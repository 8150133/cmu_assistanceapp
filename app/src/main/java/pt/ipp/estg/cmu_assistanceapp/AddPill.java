package pt.ipp.estg.cmu_assistanceapp;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class AddPill extends AppCompatActivity implements View.OnClickListener {

    String todayDate;

    EditText editText;
    EditText editText2;
    Spinner spinner;
    Button saveBtn;
    ImageButton camera;
    String CHANNEL_ID = "personal_notification";
    int NOTIFICATION_ID = 001;

    private ArrayList addPillArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pill);

        editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        spinner = (Spinner) findViewById(R.id.spinner);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        saveBtn.setOnClickListener(this);

        camera = (ImageButton) findViewById(R.id.camera_button);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toCameraIntent = new Intent(AddPill.this, CameraActivity.class);
                startActivity(toCameraIntent);
            }
        });


        addPillArrayList = new ArrayList();

        getTodayDate();
    }

    public void getTodayDate() {

        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy");
        String dateString = sdf.format(date);
        todayDate = dateString;

    }

    @Override
    public void onClick(View v) {
        // Get the values provided by the user via the UI
        String providedNameOfPill = editText.getText().toString();
        String providedDosage = editText2.getText().toString();
        String providedFrequency = spinner.getSelectedItem().toString();


        // Pass above values to the setter methods in POJO class
        DetailsPill detailsPillObj = new DetailsPill();
        detailsPillObj.setNameOfPill(providedNameOfPill);
        detailsPillObj.setDosage(providedDosage);
        detailsPillObj.setFrequency(providedFrequency);
        detailsPillObj.setDate(todayDate);

        // Add a pill with all details to an ArrayList
        addPillArrayList.add(detailsPillObj);

        // Inserting pill details to the database is done in a separate method
        insertPill(detailsPillObj);
        notification("ADD MEDICATION");


        finish();   //THIS WILL RETURN TO ORIGINAL SCREEN
    }

    public void insertPill(DetailsPill paraDetailsPillObj) {

        // First I have to open the DBHelper class by creating a new object of that
        AndroidOpenDBHelper androidOpenDbHelperObj = new AndroidOpenDBHelper(this);

        // Get a writable SQLite database, because I'm going to insert some values
        // SQLiteDatabase has methods to create, delete, execute SQL commands, and perform other common database management tasks.
        SQLiteDatabase sqliteDatabase = androidOpenDbHelperObj.getWritableDatabase();

        // ContentValues class is used to store a set of values that the ContentResolver can process.
        ContentValues contentValues = new ContentValues();

        // Get values from the POJO class and passing them to the ContentValues class
        contentValues.put(AndroidOpenDBHelper.COLUMN_NAME_NAME_OF_PILL, paraDetailsPillObj.getNameOfPill());
        contentValues.put(AndroidOpenDBHelper.COLUMN_NAME_DOSAGE, paraDetailsPillObj.getDosage());
        contentValues.put(AndroidOpenDBHelper.COLLUMN_NAME_FREQUENCY, paraDetailsPillObj.getFrequency());
        contentValues.put(AndroidOpenDBHelper.COLUMN_NAME_DATE, paraDetailsPillObj.getDate());
        contentValues.put(AndroidOpenDBHelper.COLUMN_NAME_MORNING_STATUS_PENDING, "yes");
        contentValues.put(AndroidOpenDBHelper.COLUMN_NAME_NOON_STATUS_PENDING, "yes");
        contentValues.put(AndroidOpenDBHelper.COLUMN_NAME_NIGHT_STATUS_PENDING, "yes");

        // Now I can insert the data in to relevant table
        // I am going pass the id value, which is going to change because of the insert method, to a long variable to show in Toast
        long affectedColumnId = sqliteDatabase.insert(AndroidOpenDBHelper.TABLE_NAME_ADD_PILL, null, contentValues);

        // It is a good practice to close the database connections after you're done with it
        sqliteDatabase.close();

        Toast.makeText(this, "Values inserted column ID is :" + affectedColumnId, Toast.LENGTH_SHORT).show();

    }

    public void notification(String titulo) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Medication added")
                .setContentText("Medication Added Succesfully")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        creatNotificationChannel();


        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, mBuilder.build());

    }

    private void creatNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Login Notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

    }
}

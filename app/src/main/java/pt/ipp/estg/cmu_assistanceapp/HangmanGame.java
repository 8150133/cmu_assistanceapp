package pt.ipp.estg.cmu_assistanceapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class HangmanGame extends AppCompatActivity {
    String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    TextView initialABC;
    String[] movieList = new String[20];
    String word = initList(movieList).toUpperCase();
    TextView phrase;
    String underScores = "";
    String newUnderScores = "";
    int counter;
    TextView numWrong;
    String status = "";
    EditText input;
    Button b;
    int numMistakesAllowed = 5;
    private ImageView[] bodyParts = new ImageView[numMistakesAllowed];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //SecondClass.updateActivity(this);
        setContentView(R.layout.hangman_game_layout);
        b = (Button) this.findViewById(R.id.button1);
        initialABC = (TextView) this.findViewById(R.id.initialABC);
        initialABC.setText(abc);
        initGraphics();
        phrase = (TextView) this.findViewById(R.id.phraseid);
        input = (EditText) findViewById(R.id.answer);
        newUnderScores = underScores(word, underScores);
        phrase.setText(newUnderScores);
        numWrong = (TextView) findViewById(R.id.numWrong);
        numWrong.setText("Number Wrong: " + counter);
        submitButton();
        playAgainButton();
    }

    public void playAgainButton() {
        Button playAgain = (Button) this.findViewById(R.id.buttonPlay);
        playAgain.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter = 0;
                abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                word = initList(movieList).toUpperCase();
                setContentView(R.layout.hangman_game_layout);
                initialABC = (TextView) findViewById(R.id.initialABC);
                initialABC.setText(abc);
                input = (EditText) findViewById(R.id.answer);
                phrase = (TextView) findViewById(R.id.phraseid);
                newUnderScores = underScores(word, underScores);
                phrase.setText(newUnderScores);
                numWrong = (TextView) findViewById(R.id.numWrong);
                numWrong.setText("Number Wrong: " + counter);

                submitButton();

                playAgainButton();
            }
        });
    }

    public void submitButton() {
        Button b = (Button) this.findViewById(R.id.button1);
        b.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView resp = (TextView) findViewById(R.id.response);
                input.setOnClickListener(this);
                String guess = input.getText().toString();
                input.setText("");


                guess = guessCheck(guess, abc, newUnderScores, input);
                if (abc.contains(guess) && !newUnderScores.contains(guess)) {

                    initialABC.setText("");
                    abc = changeABC(guess, abc);
                    resp.setText(abc);

                    // changes the word into underscores
                    newUnderScores = guessResults(guess, word, newUnderScores);
                    TextView phrase = (TextView) findViewById(R.id.phraseid);
                    phrase.setText(newUnderScores);
                    counter = checkWinOrLose(guess, word, counter);
                    TextView numWrong = (TextView) findViewById(R.id.numWrong);

                    status = checkWinOrLoseString(guess, word);
                    TextView statusMessage = (TextView) findViewById(R.id.statusMessage);
                    statusMessage.setText(status);

                    numWrong.setText("Number Wrong: " + counter);

                    if (counter >= 5) {
                        gameOver(false);
                    }

                    if (!newUnderScores.contains("*")) {
                        gameOver(true);
                    }

                }
            }
        });
    }

    public void initGraphics() {
        bodyParts[0] = (ImageView) findViewById(R.id.hanger);
        bodyParts[1] = (ImageView) findViewById(R.id.head);
        bodyParts[2] = (ImageView) findViewById(R.id.body);
        bodyParts[3] = (ImageView) findViewById(R.id.arms);
        bodyParts[4] = (ImageView) findViewById(R.id.legs);
    }

    //Converts strings to asterisks, spaces to underscores
    public static String underScores(String word, String underScores) {
        for (int index = 0; index < word.length(); index++) {
            if (word.charAt(index) == ' ') {
                underScores += "_";
            } else
                underScores += "*";
        }
        return underScores;
    }

    //Checks user input to be valid
    public static String guessCheck(String guess, String abc, String phrase, EditText input) {
        if (guess.length() >= 1) { // changes a word to the first letter
            guess = guess.charAt(0) + "";
            guess = guess.toUpperCase();
        }

        // checks if the guess is at least one character, part of the alphabet,
        // or hasn't already been guessed
        if (!abc.contains(guess) && !phrase.contains(guess)) {
            guess = input.getText().toString();
            guess = guess.toUpperCase();
        }
        return guess;
    }


    // changes the current phrase to insert the guess if it matches a letter in
    // the secret phrase
    public static String newCurrentPhrase(String guess, String phrase, String newUnderScores) {
        String result = "";

        for (int i = 0; i < phrase.length(); i++) {
            String letterString = phrase.charAt(i) + "";
            if (letterString.equals(guess)) {
                result += guess;
            } else if (letterString.equals(" ")) {
                result += "_";
            } else {
                result += newUnderScores.charAt(i);
            }
        }
        return result;
    }


    // checks the guess and makes a new current phrase if the guess was a letter
    // of the secret phrase
    public static String guessResults(String guess, String secretPhrase, String currentPhrase) {
        if (secretPhrase.contains(guess)) {
            currentPhrase = newCurrentPhrase(guess, secretPhrase, currentPhrase);
        }
        return currentPhrase;
    }

    //Not only initializes but also returns the randomly chosen movie
    public static String initList(String[] movieList) {
        movieList[0] = "CMU";


        Random r = new Random();
        int choose = r.nextInt(1);
        return movieList[choose];
    }

    // changes the abc string based off of the guess
    //Basically cutting the alphabet from A to guessed letter to Z
    public static String changeABC(String guess, String abc) {
        String beginning = abc.substring(0, abc.indexOf(guess));
        String end = "";

        //Exception for Z so we don't have out of bounds error
        if (!guess.equals("Z")) {
            end = abc.substring(abc.indexOf(guess) + 1);
        } else {
            beginning = abc.substring(0, abc.indexOf(guess));
        }
        return beginning + end;
    }

    // checks to see if the guess was a letter of the secret phrase and then
    // increments counter if guess is wrong, and leaves it alone if it is correct

    public int checkWinOrLose(String guess, String phrase, int counter) {
        if (!phrase.contains(guess)) {
            bodyParts[counter].setVisibility(View.VISIBLE);
            counter++;
        }
        return counter;
    }

    //Gives user status update on whether their answer was in the secret phrase
    public static String checkWinOrLoseString(String guess, String phrase) {
        if (!phrase.contains(guess)) {
            return "Incorrect Answer. Try Again.";
        }
        return "Correct Answer! Good job.";
    }

    //this is where the game ends
    public void gameOver(boolean win) {
        Button b = (Button) this.findViewById(R.id.button1);
        TextView newStatus = (TextView) findViewById(R.id.statusMessage);
        newStatus.setText("No more attempts can be made");
        b.setEnabled(false);
        if (win) {
            TextView youWin = (TextView) findViewById(R.id.youWin);
            youWin.setText("You Won");
        } else {
            TextView phrase = (TextView) findViewById(R.id.phraseid);
            phrase.setText("The phrase was " + word);
            TextView youWin = (TextView) findViewById(R.id.youWin);
            youWin.setText("You Lost");
        }
    }
}
package pt.ipp.estg.cmu_assistanceapp.API;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pharmacies {
    @SerializedName("place_id")
    @Expose
    public int place_id;

    @SerializedName("osm_type")
    @Expose
    public String osm_type;

    @SerializedName("osm_id")
    @Expose
    public int osm_id;

    @SerializedName("lat")
    @Expose
    public double lat;

    @SerializedName("lon")
    @Expose
    public double lon;

    @SerializedName("display_name")
    @Expose
    public String display_name;

    public int getPlace_id() {
        return place_id;
    }

    public void setPlace_id(int place_id) {
        this.place_id = place_id;
    }

    public String getOsm_type() {
        return osm_type;
    }

    public void setOsm_type(String osm_type) {
        this.osm_type = osm_type;
    }

    public int getOsm_id() {
        return osm_id;
    }

    public void setOsm_id(int osm_id) {
        this.osm_id = osm_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }
}

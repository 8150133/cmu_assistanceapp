package pt.ipp.estg.cmu_assistanceapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import pt.ipp.estg.cmu_assistanceapp.API.PharmacyAdapter;
import pt.ipp.estg.cmu_assistanceapp.Models.StreetMap;

public class MapaPrincipalPharmacies extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap mMap;
    private static final int REQUEST_USER_LOCATION_CODE = 99;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private Marker currentLocation;
    private Marker userMarker;
    private Marker poiMarker;
    private ImageButton logout;
    private ImageButton postsRecyclerView;
    double lati = 0.0;
    double longi = 0.0;
    double latitude = 0.0;
    double longitude = 0.0;
    RecyclerView rvPharmacies;
    PharmacyAdapter pharmacyAdapter;
    TextView cityName;
    ImageView map;
    private static final String BASE_URL = "https://nominatim.openstreetmap.org/search?";
    String displayName;
    private Marker myMarker;
    Long placeId;
    String osm_ids = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_principal_pharmacies);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        placeId = getIntent().getExtras().getLong("place_id");
        latitude = getIntent().getExtras().getDouble("latitude_id");
        longitude = getIntent().getExtras().getDouble("longitude_id");
        displayName = getIntent().getExtras().getString("display_name");
        osm_ids=getIntent().getExtras().getString("osm_ids_key");



        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Context context = null;

                LatLng latLng = new LatLng(latitude, longitude);
        myMarker = googleMap.addMarker(new MarkerOptions().position(latLng)
                .title(displayName));
        myMarker.showInfoWindow();
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));



        googleMap.setOnMarkerClickListener(this);
        final Context finalContext = context;
        mMap.setOnInfoWindowClickListener(
                new GoogleMap.OnInfoWindowClickListener() {
                    public void onInfoWindowClick(Marker marker) {
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", marker.getPosition().latitude, marker.getPosition().longitude);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        finalContext.startActivity(intent);
                    }
                }
        );
    }

    private void addPOIMarker(List<StreetMap> poi) {

        for (int i = 0; i < poi.size(); i++) {
            LatLng latLng = new LatLng(poi.get(i).getLat(), poi.get(i).getLon());


            poiMarker = mMap.addMarker(new MarkerOptions()
                    .position(latLng));

            poiMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

    }

    private void addMarker(double lati, double longi) {
        LatLng coordenadas = new LatLng(lati, longi);
        CameraUpdate myLocation = CameraUpdateFactory.newLatLngZoom(coordenadas, 16);
        if (userMarker != null) userMarker.remove();
        userMarker = mMap.addMarker(new MarkerOptions().position(coordenadas).title("UserLocation"));
        mMap.animateCamera(myLocation);
    }

    private void updateUserPosition(Location location) {
        if (location != null) {
            lati = location.getLatitude();
            longi = location.getLongitude();
            addMarker(lati, longi);
        }
    }

    private void updatePostPosition(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            updateUserPosition(location);
            updatePostPosition(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private void myLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        updateUserPosition(location);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 0, locationListener);
        callAPI();
    }

    public void callAPI() {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker.equals(myMarker)) {

            //For LookUp Api
            Intent intent = new Intent(MapaPrincipalPharmacies.this, PharmaciesDetailedActivityFromLookUpApi.class);
            intent.putExtra("osm_ids_key", osm_ids);
            startActivity(intent);
        }
        return true;

    }
}
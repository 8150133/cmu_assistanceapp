package pt.ipp.estg.cmu_assistanceapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


import pt.ipp.estg.cmu_assistanceapp.DeletePill;
import pt.ipp.estg.cmu_assistanceapp.Models.PillDetail;
import pt.ipp.estg.cmu_assistanceapp.R;
import pt.ipp.estg.cmu_assistanceapp.Schedule;


public class DeletePillAdapter extends BaseAdapter {

    List<PillDetail> pills;
    Activity context;
    LayoutInflater inflater;

    public DeletePillAdapter(Activity context, List<PillDetail> pills) {

        this.context = context;
        this.pills = pills;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pills.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.delete_pill_list_item, parent, false);
            holder = new ViewHolder();
            holder.tvPillName = (TextView) convertView.findViewById(R.id.tv_pill_title);

            holder.ibDelete = (ImageButton) convertView.findViewById(R.id.btn_delete);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PillDetail pill = pills.get(position);

        holder.ibDelete.setTag(position);

        holder.tvPillName.setText(pill.getPillName());
        holder.ibDelete.setOnClickListener(clickListener);

        return convertView;
    }

    public static class ViewHolder {
        TextView tvPillName;

        ImageView ibDelete;
    }

    public void updateCompletedValue(int position, boolean status) {

        Schedule activity = (Schedule) context;
        View view = (View) activity.listView.getChildAt(position);
        TextView tvCompleted = (TextView) view.findViewById(R.id.tv_completed);
        tvCompleted.setText(status ? "YES" : "NO");

    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            DeletePill activity = (DeletePill) context;

            int position = (int) view.getTag();
            PillDetail pill = pills.get(position);

            pill.setMorningPending(false);
            pills.set(position, pill);

            activity.updateRecord(pill);
        }
    };
}

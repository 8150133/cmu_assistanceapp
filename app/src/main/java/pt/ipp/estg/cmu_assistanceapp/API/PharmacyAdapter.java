package pt.ipp.estg.cmu_assistanceapp.API;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pt.ipp.estg.cmu_assistanceapp.PharmacyActivity;
import pt.ipp.estg.cmu_assistanceapp.R;

public class PharmacyAdapter extends  RecyclerView.Adapter<PharmacyAdapter.ViewHolder>{
    private Context mContext;
    private List<Pharmacies> mListPharmacies;

    public PharmacyAdapter(Context mContext, List<Pharmacies> mListPharmacies) {
        this.mContext = mContext;
        this.mListPharmacies = mListPharmacies;
    }

    @NonNull
    @Override
    public PharmacyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View pharmaciesView = inflater.inflate(R.layout.item_pharmacy, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(pharmaciesView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PharmacyAdapter.ViewHolder viewHolder, int i) {
        final Pharmacies apiPC = mListPharmacies.get(i);

        TextView textPostos = viewHolder.nPharmacies;
        TextView textLatitude = viewHolder.latitude;
        TextView textLongitude = viewHolder.longitude;

        textPostos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PharmacyActivity.class);
                String nPharmacie = String.valueOf(apiPC.getDisplay_name());
                String latitude = String.valueOf(apiPC.getLat());
                String longitude = String.valueOf(apiPC.getLon());

                intent.putExtra("farmacia",nPharmacie);
                intent.putExtra("latitude",latitude);
                intent.putExtra("longitude",longitude);

                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListPharmacies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nPharmacies;

        public TextView latitude;
        public TextView longitude;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nPharmacies = (TextView) itemView.findViewById(R.id.nomePOI);
            latitude = (TextView) itemView.findViewById(R.id.latitude);
            longitude = (TextView) itemView.findViewById(R.id.longitude);
        }
    }
}

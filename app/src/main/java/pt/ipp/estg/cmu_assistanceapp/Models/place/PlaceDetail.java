
package pt.ipp.estg.cmu_assistanceapp.Models.place;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceDetail {

    @SerializedName("place_id")
    @Expose
    private Long placeId;
    @SerializedName("parent_place_id")
    @Expose
    private Integer parentPlaceId;
    @SerializedName("osm_type")
    @Expose
    private String osmType;
    @SerializedName("osm_id")
    @Expose
    private Long osmId;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("admin_level")
    @Expose
    private Long adminLevel;

    @SerializedName("addresstags")
    @Expose
    private List<Object> addresstags = null;
    @SerializedName("housenumber")
    @Expose
    private Object housenumber;
    @SerializedName("calculated_postcode")
    @Expose
    private String calculatedPostcode;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("indexed_date")
    @Expose
    private String indexedDate;
    @SerializedName("importance")
    @Expose
    private Long importance;
    @SerializedName("calculated_importance")
    @Expose
    private Long calculatedImportance;

    @SerializedName("calculated_wikipedia")
    @Expose
    private Object calculatedWikipedia;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("rank_address")
    @Expose
    private Integer rankAddress;
    @SerializedName("rank_search")
    @Expose
    private Integer rankSearch;
    @SerializedName("isarea")
    @Expose
    private Boolean isarea;
    @SerializedName("centroid")
    @Expose
    private Centroid centroid;
    @SerializedName("geometry")
    @Expose
    private Geometry geometry;

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public Integer getParentPlaceId() {
        return parentPlaceId;
    }

    public void setParentPlaceId(Integer parentPlaceId) {
        this.parentPlaceId = parentPlaceId;
    }

    public String getOsmType() {
        return osmType;
    }

    public void setOsmType(String osmType) {
        this.osmType = osmType;
    }

    public Long getOsmId() {
        return osmId;
    }

    public void setOsmId(Long osmId) {
        this.osmId = osmId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getAdminLevel() {
        return adminLevel;
    }

    public void setAdminLevel(Long adminLevel) {
        this.adminLevel = adminLevel;
    }





    public List<Object> getAddresstags() {
        return addresstags;
    }

    public void setAddresstags(List<Object> addresstags) {
        this.addresstags = addresstags;
    }

    public Object getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(Object housenumber) {
        this.housenumber = housenumber;
    }

    public String getCalculatedPostcode() {
        return calculatedPostcode;
    }

    public void setCalculatedPostcode(String calculatedPostcode) {
        this.calculatedPostcode = calculatedPostcode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getIndexedDate() {
        return indexedDate;
    }

    public void setIndexedDate(String indexedDate) {
        this.indexedDate = indexedDate;
    }

    public Long getImportance() {
        return importance;
    }

    public void setImportance(Long importance) {
        this.importance = importance;
    }

    public Long getCalculatedImportance() {
        return calculatedImportance;
    }

    public void setCalculatedImportance(Long calculatedImportance) {
        this.calculatedImportance = calculatedImportance;
    }



    public Object getCalculatedWikipedia() {
        return calculatedWikipedia;
    }

    public void setCalculatedWikipedia(Object calculatedWikipedia) {
        this.calculatedWikipedia = calculatedWikipedia;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getRankAddress() {
        return rankAddress;
    }

    public void setRankAddress(Integer rankAddress) {
        this.rankAddress = rankAddress;
    }

    public Integer getRankSearch() {
        return rankSearch;
    }

    public void setRankSearch(Integer rankSearch) {
        this.rankSearch = rankSearch;
    }

    public Boolean getIsarea() {
        return isarea;
    }

    public void setIsarea(Boolean isarea) {
        this.isarea = isarea;
    }

    public Centroid getCentroid() {
        return centroid;
    }

    public void setCentroid(Centroid centroid) {
        this.centroid = centroid;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}

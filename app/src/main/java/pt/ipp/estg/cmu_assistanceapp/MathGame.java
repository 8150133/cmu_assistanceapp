package pt.ipp.estg.cmu_assistanceapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MathGame extends AppCompatActivity {
    Button answer1, answer2, answer3, answer4;
    TextView scoreTV, questionTV;

    private QuestionModel nQuestions = new QuestionModel();
    private String answer;
    private int score = 0;
    private int questionsLength = nQuestions.questions.length;
    Random r;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mathgame_layout);

        r = new Random();

        answer1 = findViewById(R.id.answer1);
        answer2 = findViewById(R.id.answer2);
        answer3 = findViewById(R.id.answer3);
        answer4 = findViewById(R.id.answer4);


        scoreTV = findViewById(R.id.score);
        questionTV = findViewById(R.id.question);

        scoreTV.setText("Score: " + score);
        updateQuestion(r.nextInt(questionsLength));

        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer1.getText() == answer) {
                    score++;
                    scoreTV.setText("Score: " + score);
                    updateQuestion(r.nextInt(questionsLength));
                } else {
                    gameOver();
                }
            }
        });

        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer2.getText() == answer) {
                    score++;
                    scoreTV.setText("Score: " + score);
                    updateQuestion(r.nextInt(questionsLength));
                } else {
                    gameOver();
                }
            }
        });

        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer3.getText() == answer) {
                    score++;
                    scoreTV.setText("Score: " + score);
                    updateQuestion(r.nextInt(questionsLength));
                } else {
                    gameOver();
                }
            }
        });

        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (answer4.getText() == answer) {
                    score++;
                    scoreTV.setText("Score: " + score);
                    updateQuestion(r.nextInt(questionsLength));
                } else {
                    gameOver();
                }
            }
        });

    }

    private void gameOver() {
        AlertDialog.Builder aBuilder = new AlertDialog.Builder(MathGame.this);
        aBuilder.setMessage("Game Over. Your score was: " + score)
                .setCancelable(false)
                .setPositiveButton("New Game", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(getApplicationContext(), MathGame.class));
                        finish();
                    }
                })
                .setNegativeButton("Choose Another Game", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent toChooseGameIntent = new Intent(MathGame.this, GameChoose.class);
                        startActivity(toChooseGameIntent);
                        finish();
                    }
                });
        AlertDialog alertDialog = aBuilder.create();
        alertDialog.show();
    }

    private void updateQuestion(int num) {
        questionTV.setText(nQuestions.getQuestion(num));
        answer1.setText(nQuestions.getChohice1(num));
        answer2.setText(nQuestions.getChohice2(num));
        answer3.setText(nQuestions.getChohice3(num));
        answer4.setText(nQuestions.getChohice4(num));
        answer = nQuestions.getCCorrectAnswer(num);
    }
}

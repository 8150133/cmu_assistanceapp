package pt.ipp.estg.cmu_assistanceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.firestore.FirebaseFirestore;


import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;



public class MainActivity extends AppCompatActivity {

    URL imageURL = null;
    ImageView logoImage = null;
    InputStream is = null;
    Bitmap bmImg = null;

    FirebaseAuth fAuth;
    FirebaseFirestore fStore;

    Button btToLogin, btnDownload;
    ProgressDialog progressDialogDownload;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        Button btToLogin = findViewById(R.id.btToLogin);
        btToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentToLogin = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intentToLogin);
            }
        });

        Button btnDownload = findViewById(R.id.btDownload);
        logoImage = findViewById(R.id.logo);

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTaskImage downloadImage = new AsyncTaskImage();
                downloadImage.execute("https://cdn.dribbble.com/users/125948/screenshots/2602551/medical-icons-2.png");

            }
        });
    }

    private class AsyncTaskImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogDownload = new ProgressDialog(MainActivity.this);
            progressDialogDownload.setMessage("Downloading");
            progressDialogDownload.setIndeterminate(false);
            progressDialogDownload.setCancelable(false);
            progressDialogDownload.show();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                imageURL = new URL(strings[0]);
                HttpURLConnection conn = (HttpURLConnection) imageURL.openConnection();
                conn.setDoInput(true);
                conn.connect();
                is = conn.getInputStream();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                bmImg = BitmapFactory.decodeStream(is, null, options);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bmImg;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (logoImage != null) {
                progressDialogDownload.hide();
                logoImage.setImageBitmap(bitmap);
            } else {
                progressDialogDownload.show();
            }
        }
    }
}

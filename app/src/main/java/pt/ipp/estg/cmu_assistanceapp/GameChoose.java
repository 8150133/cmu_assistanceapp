package pt.ipp.estg.cmu_assistanceapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class GameChoose extends AppCompatActivity {
    ImageButton hangmangame, mathgame;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choosegame_layout);

        hangmangame =findViewById(R.id.memorygame);
        hangmangame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToMemoryGame = new Intent(GameChoose.this, HangmanGame.class);
                startActivity(intentToMemoryGame);
            }
        });

        mathgame =findViewById(R.id.mathgame);
        mathgame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToMathGame = new Intent(GameChoose.this, MathGame.class);
                startActivity(intentToMathGame);
            }
        });
    }
}

package pt.ipp.estg.cmu_assistanceapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import pt.ipp.estg.cmu_assistanceapp.API.APIClient;
import pt.ipp.estg.cmu_assistanceapp.API.OSMAPI;
import pt.ipp.estg.cmu_assistanceapp.Models.lookup.LookUp;
import pt.ipp.estg.cmu_assistanceapp.Utils.Connectivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PharmaciesDetailedActivityFromLookUpApi extends AppCompatActivity {
    Long placeId;
    OSMAPI osmAPI;
    TextView placeID, osmId, lat, lon, displayName, type, road, city, county, country;
    String osm_ids;
    ProgressDialog progressDoalog;
    private ProgressDialog progressDialog;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_from_look_up_api_pharmacies);

        osmAPI = APIClient.getClient().create(OSMAPI.class);
        initView();
        osm_ids = getIntent().getExtras().getString("osm_ids_key");

        if (Connectivity.isConnected(PharmaciesDetailedActivityFromLookUpApi.this)) {
            showProgress();
            Call<List<LookUp>> callUrl = osmAPI.getLocationDetailFromLookUp("/lookup?osm_ids=" + osm_ids + "&format=json");

            callUrl.enqueue(new Callback<List<LookUp>>() {
                @Override
                public void onResponse(Call<List<LookUp>> call, Response<List<LookUp>> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            placeID.setText("Place ID : " + response.body().get(0).getPlaceId());
                            osmId.setText("OSM ID : " + response.body().get(0).getOsmId());
                            lat.setText("Latitude : " + response.body().get(0).getLat());
                            lon.setText("Longitude : " + response.body().get(0).getLon());
                            displayName.setText("Name :" + response.body().get(0).getDisplayName());
                            type.setText("Type :" + response.body().get(0).getType());
                            if (response.body().get(0).getAddress() != null) {
                                road.setText("Road : " + response.body().get(0).getAddress().getRoad());
                                city.setText("City : " + response.body().get(0).getAddress().getCity());
                                county.setText("County : " + response.body().get(0).getAddress().getCounty());
                                country.setText("Country : " + response.body().get(0).getAddress().getCountry());
                            }

                        }
                    }
                    dissmissProgress();

                }

                @Override
                public void onFailure(Call<List<LookUp>> call, Throwable t) {
                    dissmissProgress();
                    Toast.makeText(getApplicationContext(), "Error in Response:" + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dissmissProgress();
            Toast.makeText(PharmaciesDetailedActivityFromLookUpApi.this, "Your internet connection seems off!", Toast.LENGTH_SHORT).show();
        }


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Registering...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

    }



    public void initView() {
        placeID = findViewById(R.id.place_Id);
        osmId = findViewById(R.id.osmId);
        lat = findViewById(R.id.lat);
        lon = findViewById(R.id.lon);
        displayName = findViewById(R.id.displayName);
        type = findViewById(R.id.type);
        road = findViewById(R.id.road);
        city = findViewById(R.id.city);
        county = findViewById(R.id.county);
        country = findViewById(R.id.country);

    }

    public void showProgress() {
        progressDoalog = new ProgressDialog(PharmaciesDetailedActivityFromLookUpApi.this);
        progressDoalog.setMessage("Its loading....");
        progressDoalog.setTitle("Wait");
        progressDoalog.show();
    }

    public void dissmissProgress() {
        if (progressDoalog != null) {
            if (progressDoalog.isShowing()) {
                progressDoalog.dismiss();
            }
        }
    }
}
